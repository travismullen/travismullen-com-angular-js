'use strict';

describe('Service: Resume', function () {

  // load the service's module
  beforeEach(module('TravisMullenApp'));

  // instantiate service
  var Resume;
  beforeEach(inject(function (_Resume_) {
    Resume = _Resume_;
  }));

  it('should do something', function () {
    expect(!!Resume).toBe(true);
  });

});

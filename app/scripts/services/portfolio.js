'use strict';

angular.module('TravisMullenApp')
  .service('Portfolio', function Portfolio($http) {
    return {
        getProjects: function() {
            return $http.get(window.siteConfig.global.apiUrl+'api/portfolio.json').success(function(res) {
                return window.PorfolioData = res.data;
            });
        }
    }
  });

'use strict';

angular.module('TravisMullenApp')
  .service('Resume', function Resume($http) {
    return {
        getResume: function() {
            return $http.get(window.siteConfig.global.apiUrl+'api/resume.json').success(function(res) {
                return res.data;
            });
        }
    }
  });

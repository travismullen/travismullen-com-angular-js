'use strict';

angular.module('TravisMullenApp')
  .service('Project', function Project($http) {
    return {
        getProject: function(localUrl) {
            return $http.get(window.siteConfig.global.apiUrl+'api/'+localUrl+'.json').success(function(res) {
                return res;
            });
        }
    }
  });

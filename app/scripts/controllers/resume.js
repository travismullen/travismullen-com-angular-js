'use strict';

angular.module('TravisMullenApp')
  .controller('ResumeCtrl', function ($scope, Resume) {
    Resume.getResume().then(function(res) {
        $scope.resume = res.data;
    });
  });

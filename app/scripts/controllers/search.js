'use strict';

angular.module('TravisMullenApp')
  .controller('SearchCtrl', function ($scope, Portfolio) {
        Portfolio.getProjects().then(function(res) {
            $scope.items = res.data;
        });
  });

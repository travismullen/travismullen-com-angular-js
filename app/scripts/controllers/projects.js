'use strict';

angular.module('TravisMullenApp')
    .controller('ProjectsCtrl', function ($scope, $location, Portfolio) {
        $scope.location = $location;
        Portfolio.getProjects().then(function(res) {
            $scope.projects = res.data;
        });
    });

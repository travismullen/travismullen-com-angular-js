'use strict';

angular.module('TravisMullenApp')
    .controller('ProjectCtrl', function ($scope, $location, $routeParams, Portfolio, Project) {
        $scope.id = $routeParams.itemId;
        $scope.location = $location;
        // console.log("$routeParams.itemId",$routeParams.itemId);
        Portfolio.getProjects().then(function(res) {
            $scope.projects = res.data;

            $scope.projectId = _.findKey($scope.projects, { 'localUrl' : $scope.id }) || _.findKey($scope.projects, { 'id' : $scope.id });

            if ($scope.projectId) {
                $scope.project = $scope.projects[$scope.projectId];
                Project.getProject($scope.project.localUrl).success(function(res) {
                    $scope.projectData = res;
                });
            } else {
                $scope.project = { 'title' : 'Project cannot be found.', 'desc' : 'Please try a different URL' };
            }
        });

    });

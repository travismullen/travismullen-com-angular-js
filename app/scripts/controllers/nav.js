'use strict';

angular.module('TravisMullenApp')
  .controller('NavCtrl', function ($scope, Portfolio, Resume) {
        Portfolio.getProjects().then(function(res) {
            $scope.nav = res.data;
        });
        Resume.getResume().then(function(res) {
            $scope.resume = res.data;
        });
  });